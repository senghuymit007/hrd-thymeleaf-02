package com.howtodoinjava.demo.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class I18nConfiguration {
    @Configuration
    @EnableWebMvc
    public class I18nConfigu extends WebMvcConfigurerAdapter {

        @Bean
        public MessageSource messageSource() {
            ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
            messageSource.setBasename("i18n/messages");
            messageSource.setDefaultEncoding("UTF-8");
            return messageSource;
        }

    }
}
