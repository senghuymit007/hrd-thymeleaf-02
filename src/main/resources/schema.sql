DROP TABLE IF EXISTS tbl_article;

CREATE TABLE tbl_article (
                             id INT AUTO_INCREMENT  PRIMARY KEY,
                             title VARCHAR(250) NOT NULL,
                             description VARCHAR(250) NOT NULL,
                             img VARCHAR(250) DEFAULT NULL
);